﻿using EcommerceSampleForUnitTest.Models;

namespace EcommerceSampleForUnitTest.Services.Interfaces
{
	public interface IShoppingService
	{
		IEnumerable<ShoppingItem> GetAllItems();
		ShoppingItem Add(ShoppingItem newItem);
		ShoppingItem GetById(Guid id);
		void Remove(Guid id);
	}
}
