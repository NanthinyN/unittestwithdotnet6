﻿using EcommerceSampleForUnitTest.Models;
using EcommerceSampleForUnitTest.Services.Interfaces;

namespace EcommerceSampleForUnitTest.Services
{
	public class ShoppingService : IShoppingService
	{
		private readonly List<ShoppingItem> _shoppingCart;
		public ShoppingService()
		{
			_shoppingCart = new List<ShoppingItem>()
			{
				new ShoppingItem() { Id = new Guid("ab2bd817-98cd-4cf3-a80a-53ea0cd9c200"),
					Name = "Orange Juice", Manufacturer="Orange Tree", Price = 5.00M },
				new ShoppingItem() { Id = new Guid("815accac-fd5b-478a-a9d6-f171a2f6ae7f"),
					Name = "Diary Milk", Manufacturer="Cow", Price = 4.00M },
				new ShoppingItem() { Id = new Guid("33704c4a-5b87-464c-bfb6-51971b4d18ad"),
					Name = "Frozen Pizza", Manufacturer="Uncle Mickey", Price = 12.00M }
			};
		}
		public ShoppingItem Add(ShoppingItem newItem)
		{
			newItem.Id = Guid.NewGuid();
			_shoppingCart.Add(newItem);
			return newItem;
		}

		public IEnumerable<ShoppingItem> GetAllItems()
		{
			return _shoppingCart;
		}

		public ShoppingItem GetById(Guid id)
		{
			var item = _shoppingCart.Where(c=>c.Id==id).FirstOrDefault();
			return item; 
		}

		public void Remove(Guid id)
		{
			var cart = _shoppingCart.First(c=>c.Id==id);
			_shoppingCart.Remove(cart);
		}
	}
}
